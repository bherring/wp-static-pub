<?php
class StaticPubSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Static Pub', 
            'Static Pub', 
            'manage_options', 
            'static-pub-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'static_pub_options' );
        ?>
        <div class="wrap">
            <h1>Static Pub Settings</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'static_pub_options_group' );
                do_settings_sections( 'static-pub-admin' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'static_pub_options_group', // Option group
            'static_pub_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'jenkins_setting_section_id', // ID
            'Jenkins Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'static-pub-admin' // Page
        );  

        add_settings_field(
            'jenkins_url', // ID
            'Jenkins Trigger URL', // Title 
            array( $this, 'jenkins_url_callback' ), // Callback
            'static-pub-admin', // Page
            'jenkins_setting_section_id' // Section           
        );   
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['jenkins_url'] ) )
            $new_input['jenkins_url'] = sanitize_text_field( $input['jenkins_url'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        #print 'Enter your settings below:';
    }
    
    /** 
     * Get the settings option array and print one of its values
     */
    public function jenkins_url_callback()
    {
        printf(
            '<input type="text" id="jenkins_url" name="static_pub_options[jenkins_url]" value="%s" />',
            isset( $this->options['jenkins_url'] ) ? esc_attr( $this->options['jenkins_url']) : ''
        );
    }
}

if( is_admin() )
    $static_pub_settings_page = new StaticPubSettingsPage();
