<?php
add_action('admin_bar_menu', 'static_pub_setup_admin_bar');

function static_pub_setup_admin_bar($admin_bar) {
  $admin_bar->add_menu( array(
        'id'      => 'static-pub-publish',
        'parent'  => 'top-secondary',
        'title'   => 'Publish',
        'href'    => "#",
        'meta'    => array(
        'title'   => __('Publish'),
      ),
  ) );
}


add_action( 'admin_footer', 'static_pub_publish_javascript' ); // Write our JS below here

function static_pub_publish_javascript() { ?>
	<script type="text/javascript" >
	jQuery(document).ready(function($) {
	
	  var data = {
			  'action': 'static_pub_publish'
		};

		var myEl = document.getElementById('wp-admin-bar-static-pub-publish');

    myEl.addEventListener('click', function() {
        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		    jQuery.post(ajaxurl, data, function(response) {
		    });
    }, false)

		
	});
	</script> <?php
}

add_action( 'wp_ajax_static_pub_publish', 'static_pub_publish_callback' );

function static_pub_publish_callback() {
  global $options;
  
  $ch = curl_init();
  
  
   $service_url = get_option('static_pub_options', array() )['jenkins_url'];
   $curl = curl_init($service_url);
   $curl_post_data = array();
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($curl, CURLOPT_POST, true);
   curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
   $curl_response = curl_exec($curl);
   curl_close($curl);
}
?>
